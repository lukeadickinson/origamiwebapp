﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrigamiTestApp
{
   public class State
   {
       public enum possibleStates { loading, loaded, drawing, drawing_finished}

       possibleStates currentState = possibleStates.loading;

       public possibleStates CurrentState{
           get
           {
               return currentState;
           }
           set{
               currentState = value;
           }
       }
       public State()
       {

       }
   }
}
