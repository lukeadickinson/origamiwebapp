﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace OrigamiTestApp
{
    public class PenDrawing
    {
        int width = 220;
        int height = 235;

        public List<Point> MyPoints { get { return myPoints; } }
        public List<Line> MyLines { get { return myLines; } }

        List<Point> myPoints = new List<Point>();
        List<Line> myLines = new List<Line>();

        public delegate void BasicEventHandler();
        public event BasicEventHandler GraphUpdated;
        public PenDrawing()
        {
    
        }
        public PenDrawing(PenDrawing pendrawing)
        {
            //copy
            foreach(var p in pendrawing.MyPoints)
                 addPoint(p);
        }
            
        public void addPoint(Point p)
        {
            Point newP = new Point(p.X, p.Y);
            myPoints.Add(p);
            if(myPoints.Count > 1)
            {
                Line newLine = new Line();
                newLine.X1 = myPoints[myPoints.Count - 2].X;
                newLine.Y1 = myPoints[myPoints.Count - 2].Y;
                newLine.X2 = myPoints[myPoints.Count - 1].X;
                newLine.Y2 = myPoints[myPoints.Count - 1].Y;
                newLine.Stroke = System.Windows.Media.Brushes.Black;

                myLines.Add(newLine);
            }
            if (GraphUpdated != null)
                GraphUpdated();
        }
        public void undoPoint()
        {
            if (myPoints.Count > 0)
                myPoints.RemoveAt(myPoints.Count - 1);
            if (myLines.Count > 0)
                myLines.RemoveAt(myLines.Count - 1);
            if (GraphUpdated != null)
                GraphUpdated();
        }
        public void resetPoints()
        {
            myPoints.Clear();
            myLines.Clear();
            if (GraphUpdated != null)
                GraphUpdated();
        }
        public bool pointsIsEmpty()
        {
            return myPoints.Count <= 0;
        }
        public bool linesIsEmpty()
        {
            return myLines.Count <= 0;
        }
    }
}
