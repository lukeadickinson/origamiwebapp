﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace OrigamiTestApp
{
    public class LinePairs
    {
        public Line LeftLine;
        public Line RightLine;
        public Line OrginalLine;
    }
}
