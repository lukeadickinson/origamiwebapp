﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace OrigamiTestApp
{
    public class ShapeOf4Points
    {
        public Point point1;
        public Point point2;
        public Point point3;
        public Point point4;
        public bool side1;
        List<Line> list = new List<Line>();
        public List<Line> Lines { get { return list; } }
        public ShapeOf4Points(Point p1, Point p2, Point p3, Point p4)
        {
            point1 = p1;
            point2 = p2;
            point3 = p3;
            point4 = p4;

            list.Add(makeLine(point1, point2));
            list.Add(makeLine(point2, point3));
            list.Add(makeLine(point3, point4));
            list.Add(makeLine(point4, point1));
        }

        Line makeLine(Point p1 , Point p2)
        {
            Line line1 = new Line();
            line1.X1 = p1.X;
            line1.Y1 = p1.Y;
            line1.X2 = p2.X;
            line1.Y2 = p2.Y;
            line1.Stroke = System.Windows.Media.Brushes.Black;

            return line1;
        }

    }
}
