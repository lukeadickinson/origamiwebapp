﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
namespace OrigamiTestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        State applicationState = new State();
        PenDrawing penDrawing = new PenDrawing();
        PenDrawing processingDrawing = new PenDrawing();

        List<LinePairs> parallelLines = new List<LinePairs>();
        List<ShapeOf4Points> ShapeOf4PointsList = new List<ShapeOf4Points>();

        PenDrawing leftTrace = new PenDrawing();
        PenDrawing rightTrace = new PenDrawing();



        int spaceInbetween = 10;
        public MainWindow()
        {
            InitializeComponent();
            applicationState.CurrentState = State.possibleStates.loaded;
            penDrawing.GraphUpdated += penDrawing_GraphUpdated;
            setupSampleGraph();

            enableDisableDrawButtons();

        }
        void setupSampleGraph()
        {
            penDrawing.addPoint(new System.Windows.Point(18, 209));
            penDrawing.addPoint(new System.Windows.Point(77, 208));
            penDrawing.addPoint(new System.Windows.Point(74, 107));
            penDrawing.addPoint(new System.Windows.Point(118, 70));
            penDrawing.addPoint(new System.Windows.Point(150, 105));
            penDrawing.addPoint(new System.Windows.Point(163, 205));
            penDrawing.addPoint(new System.Windows.Point(205, 205));
        }
        #region drawing Canvas
        void penDrawing_GraphUpdated()
        {
            drawingCanvas.Children.Clear();
            
            foreach(var p in penDrawing.MyPoints)
            {
                var myPoint = new Ellipse ();
                myPoint.Height =5;
                myPoint.Width = 5;
                myPoint.Stroke = System.Windows.Media.Brushes.Black;
                Canvas.SetLeft(myPoint, p.X-3);
                Canvas.SetTop(myPoint, p.Y-3);
                drawingCanvas.Children.Add(myPoint);
            }
            foreach(var l in penDrawing.MyLines)
            {
                drawingCanvas.Children.Add(l);
            }
        }

        void startDrawing()
        {
            applicationState.CurrentState = State.possibleStates.drawing;
            resetDrawingCanvas();
        }
        void resetDrawingCanvas()
        {
            penDrawing.resetPoints();
            enableDisableDrawButtons();
        }
        void enableDisableDrawButtons()
        {
            if (penDrawing.pointsIsEmpty())
                btnUndoDrawing.IsEnabled = false;
            else
                btnUndoDrawing.IsEnabled = true;

            if (penDrawing.linesIsEmpty())
                btnSaveDrawing.IsEnabled = false;
            else
                btnSaveDrawing.IsEnabled = true;

        }
        void endDrawing()
        {
            saveDrawingCanvas();           
        }
        void saveDrawingCanvas()
        {
            processingDrawing = new PenDrawing(penDrawing);
            proccessImage();
        }

        private void btnSaveDrawing_Click(object sender, RoutedEventArgs e)
        {
            endDrawing();
        }

        private void btnUndoDrawing_Click(object sender, RoutedEventArgs e)
        {
            penDrawing.undoPoint();
            enableDisableDrawButtons();
        }

        private void btnResetDrawing_Click(object sender, RoutedEventArgs e)
        {
            startDrawing();
        }

        private void drawingCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var point = e.GetPosition(drawingCanvas);
            penDrawing.addPoint(point);

            enableDisableDrawButtons();
        }
        #endregion
        #region processingImage

        void proccessImage()
        {

            parallelLines.Clear();
            leftTrace = new PenDrawing();
            rightTrace = new PenDrawing();
            ProcessingCanvas.Children.Clear();
            //step two
            drawParellelLines();

            foreach (var lp in parallelLines)
            {
                ProcessingCanvas.Children.Add(lp.LeftLine);
                ProcessingCanvas.Children.Add(lp.OrginalLine);
                ProcessingCanvas.Children.Add(lp.RightLine);
            }

            //step 3/4
            //traceParellelLines(true);
            //traceParellelLines(false);

            //foreach (var l in leftTrace.MyLines)
            //{
            //    ProcessingCanvas.Children.Add(l);
            //}
            //foreach (var l in rightTrace.MyLines)
            //{
            //    ProcessingCanvas.Children.Add(l);
            //}

            //step 5
            //createShapes(leftTrace, rightTrace);

            //foreach (var shape in ShapeOf4PointsList)
            //{
            //    foreach (var line in shape.Lines)
            //    {
            //        ProcessingCanvas.Children.Add(line);
            //    }
            //}
        }
        //step 2
        void drawParellelLines()
        {
            int spaceBetween = spaceInbetween;
            for(int i=0; i <processingDrawing.MyLines.Count; i++)
            {
                var l = processingDrawing.MyLines[i];
                int valueToUse = spaceBetween;
                double slope = 1;

                if (l.X1 == l.X2){//vertial line
                    slope = 1000000;
                }
                else {
                    slope = (l.Y2 - l.Y1) / (l.X2 - l.X1);
                }

                if(Math.Abs(slope) >= 1){
                    double difference = l.Y2 - l.Y1;
                    if(difference>0){
                            valueToUse *= -1;
                    }
                }
                else{
                    double difference = l.X2 - l.X1;
                    if(difference<=0){
                        valueToUse *= -1;
                    }
                }
 
                var leftLine = createParellelLine(l, -1 * valueToUse, System.Windows.Media.Brushes.Blue);
                //add more the end to lengthen the lines
                lengthenLine(leftLine, -1 * valueToUse, i == 0, i == processingDrawing.MyLines.Count-1);

                var rightLine =createParellelLine(l, valueToUse, System.Windows.Media.Brushes.Red);
                //add more the end to lengthen the lines
                lengthenLine(rightLine, valueToUse, i == 0, i == processingDrawing.MyLines.Count - 1);

                LinePairs myLinePair = new LinePairs();
                myLinePair.LeftLine = leftLine;
                myLinePair.RightLine = rightLine;
                myLinePair.OrginalLine = l;
                parallelLines.Add(myLinePair);
            }


        }
        Line createParellelLine(Line l, int spaceBetween, System.Windows.Media.Brush brush)
        {
            Line newLine = new Line();
            newLine.Stroke = brush;
            newLine.X1 = l.X1;
            newLine.Y1 = l.Y1;
            newLine.X2 = l.X2;
            newLine.Y2 = l.Y2;

            if (newLine.X1 == newLine.X2)
            {
                newLine.X1 += spaceBetween;
                newLine.X2 += spaceBetween;
            }
            else
            {
                double slope = (newLine.Y2 - newLine.Y1) / (newLine.X2 - newLine.X1);

                //move the line up or down
                if (slope < 1 && slope >= -1)
                {
                    newLine.Y1 += spaceBetween;
                    newLine.Y2 += spaceBetween;

                }
                else
                {
                    newLine.X1 += spaceBetween;
                    newLine.X2 += spaceBetween;
                }
            }

            return newLine;
        }
        void lengthenLine(Line newLine, int spaceBetween, bool firstNode, bool lastNode)
        {
            double slope = 1;
            if (newLine.X1 == newLine.X2)
            {
                slope = Double.PositiveInfinity;
            }
            else
            {
                slope = (newLine.Y2 - newLine.Y1) / (newLine.X2 - newLine.X1);
            }
            int absSpaceBetween = Math.Abs(spaceBetween) * 3;

            if (slope == Double.PositiveInfinity)
            {
                if (newLine.Y1 < newLine.Y2)
                {
                    if (!firstNode)
                        newLine.Y1 -= absSpaceBetween;
                    if (!lastNode)
                        newLine.Y2 += absSpaceBetween;
                }
                else
                {
                    if (!firstNode)
                        newLine.Y1 += absSpaceBetween;
                    if (!lastNode)
                        newLine.Y2 -= absSpaceBetween;
                }
            }

            double x =
                    Math.Sqrt(
                        Math.Pow(absSpaceBetween, 2) /
                        (Math.Pow(slope, 2) + 1)
                    );
            double y =
                Math.Sqrt(
                    Math.Pow(absSpaceBetween, 2) -
                    Math.Pow(x, 2)
                );

            if (newLine.Y1 < newLine.Y2)
            {
                if (!firstNode)
                    newLine.Y1 -= y;
                if (!lastNode)
                    newLine.Y2 += y;
            }
            else
            {
                if (!firstNode)
                    newLine.Y1 += y;
                if (!lastNode)
                    newLine.Y2 -= y;
            }

            if (newLine.X1 < newLine.X2)
            {
                if (!firstNode)
                    newLine.X1 -= x;
                if (!lastNode)
                    newLine.X2 += x;
            }
            else
            {
                if (!firstNode)
                    newLine.X1 += x;
                if (!lastNode)
                    newLine.X2 -= x;
            }
        }
        //step 3/4
        void traceParellelLines(bool startWithLeft)
        {
            bool onLeft = startWithLeft;
            //first point
            if (startWithLeft)
                leftTrace.addPoint(new System.Windows.Point (parallelLines[0].LeftLine.X1, parallelLines[0].LeftLine.Y1));
            else
                rightTrace.addPoint(new System.Windows.Point(parallelLines[0].RightLine.X1, parallelLines[0].RightLine.Y1));

            for(int i = 1; i<parallelLines.Count; i++)
            {
                Line line1;
                Line line2;
                if (onLeft)
                    line1 = parallelLines[i - 1].LeftLine;
                else 
                    line1 = parallelLines[i - 1].RightLine;

                if (!onLeft)
                    line2 = parallelLines[i].LeftLine;
                else
                    line2 = parallelLines[i].RightLine;

                if (startWithLeft)
                    leftTrace.addPoint(intersectingPoint(line1, line2));
                else
                    rightTrace.addPoint(intersectingPoint(line1, line2));

                onLeft = !onLeft; 
            }

            System.Windows.Point lastPointToAdd;

            if(onLeft)
            {
                lastPointToAdd = new System.Windows.Point(
                            parallelLines[parallelLines.Count - 1].LeftLine.X2,
                            parallelLines[parallelLines.Count - 1].LeftLine.Y2);
            }
            else{
                lastPointToAdd = new System.Windows.Point(
                        parallelLines[parallelLines.Count - 1].RightLine.X2,
                        parallelLines[parallelLines.Count - 1].RightLine.Y2);
            }

            if (startWithLeft)
            {
                leftTrace.addPoint(lastPointToAdd);
            }
            else
            {
                rightTrace.addPoint(lastPointToAdd);
            }

        }
        System.Windows.Point intersectingPoint(Line line1, Line line2)
        {
            double intersectingY =0;
            double intersectingX =0;

            if(line1.X1 == line1.X2)
            {
                double slope2 = (line2.Y2 - line2.Y1) / (line2.X2 - line2.X1);
                double b2 =line2.Y1 - (line2.X1*slope2);
                intersectingY = intersectingX*slope2 + b2;
                intersectingX = line1.X1;
            }
            else if(line2.X1 == line2.X2)
            {
                intersectingX = line2.X1;

                double slope1 = (line1.Y2 - line1.Y1) / (line1.X2 - line1.X1);
                double b1 =line1.Y1 - (line1.X1*slope1);
                intersectingY = intersectingX*slope1 + b1;

            }
            else{
                 double slope1 = (line1.Y2 - line1.Y1) / (line1.X2 - line1.X1);
                 double slope2 = (line2.Y2 - line2.Y1) / (line2.X2 - line2.X1);
                 double b1 =line1.Y1 - (line1.X1*slope1);
                 double b2 =line2.Y1 - (line2.X1*slope2);

                 intersectingX = (b2 - b1) / (slope1 - slope2);
                 intersectingY = intersectingX*slope1 + b1;
            }
            return new System.Windows.Point(intersectingX, intersectingY);
        }
        //step5
        void createShapes(PenDrawing leftTrace, PenDrawing rightTrace)
        {
            ShapeOf4PointsList.Clear();
            bool side1 = true;
            for(int i =1; i< leftTrace.MyPoints.Count;i++)
            {
                ShapeOf4Points shape = new ShapeOf4Points(
                    leftTrace.MyPoints[i - 1],
                    rightTrace.MyPoints[i - 1],
                    rightTrace.MyPoints[i],
                    leftTrace.MyPoints[i]
                    );
                shape.side1 = side1;
                side1 = !side1;
                ShapeOf4PointsList.Add(shape);
            }
        }
        #endregion
    }
}
