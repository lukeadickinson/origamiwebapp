/**
 * Created by Luke on 11/21/2015.
 */
var ShapeStraightener = function(myCanvasName) {

    var canvasName = myCanvasName;
    var path;

    this.getPath = function()
    {
        return path;
    }

    this.init = function(){
        activatePaper(canvasName);
        path = new paper.Path();
        path.strokeColor = 'black';
    }
    this.importPath = function (svgPath){
        activatePaper(canvasName);

        paper.project.clear();
        path = paper.project.importSVG(svgPath);
    }
	this.processPath = function (flatness){
        activatePaper(canvasName);

        if(path != null) {
            path = this.bezierSubdivision(path, flatness);
            path.strokeWidth = 3;
            helper.drawColoredFirstAlternatePath(canvasName,path,"#00ff00");
            helper.drawColoredSecondAlternatePath(canvasName,path,"#FF0000");
        }
		//path = this.straightenPath(path, degreesOfFreedom);
        paper.view.draw();
    }

//old function
    this.straightenPath = function(myPath, degreesOfFreedom)
    {
        activatePaper(canvasName);

        var myDegreesOfFreedom = Number(degreesOfFreedom);
        if(myDegreesOfFreedom != null && myDegreesOfFreedom  != NaN && typeof myDegreesOfFreedom === "number")
        {
            myDegreesOfFreedom= Math.floor(degreesOfFreedom);
            if(myDegreesOfFreedom <1)
            {
                myDegreesOfFreedom =1
            }
            if(myDegreesOfFreedom >5)
            {
                myDegreesOfFreedom =5
            }
        }



		var newSegments = [myPath.segments[0]]
		for (index = 1, len = myPath.segments.length; index < len; ++index){ 
			var mySegment1 = newSegments.pop();
			var mySegment2 = myPath.segments[index]
			var segmentArray = [mySegment1,mySegment2];
			var segmentPath = new paper.Path(segmentArray);
			
			var segmentLength = segmentPath.length;
			
			var iterator = new paper.PathIterator(segmentPath, 64, 0.1),
            pos = 0,
			step = segmentLength / myDegreesOfFreedom
            //step = iterator.length / Math.ceil(iterator.length / maxDistance),
            end = segmentLength + (segmentPath._closed ? -step : step) / 2;
			
			while (pos <= end) {
				newSegments.push(new paper.Segment(iterator.getPointAt(pos)));
				pos += step;
			}
		}
		
        myPath.setSegments(newSegments);
		return myPath;
    }

    this.bezierSubdivision = function(myPath, flatness)
    {
        activatePaper(canvasName);
        var index = 0;
        while (index <myPath.curves.length){
            var myCurve = myPath.curves[index];
            if(this.isCurveWithinFlatness(myCurve, flatness))
            {
                index++;
                continue;
            }
            else {
                myPath = this.spiltBezierCurve(myPath, index);
            }
        }


        myPath.clearHandles()
        return myPath;
    }
    this.isCurveWithinFlatness = function(myCurve, flatness)
    {
        var p0=myCurve.point1;
        var p1=helper.relativeToAbsoluteHandle(myCurve.point1, myCurve.handle1);
        var p2=helper.relativeToAbsoluteHandle(myCurve.point2, myCurve.handle2);
        var p3=myCurve.point2;

        var line1 = helper.getLengthOfLine(p0,p1);
        var line2 = helper.getLengthOfLine(p1,p2);
        var line3 = helper.getLengthOfLine(p2,p3);
        var line4 = helper.getLengthOfLine(p0,p3);

        if(line1 + line2 + line3 < flatness *line4)
        {
            return true;
        }

        return false;
    }

    this.spiltBezierCurve = function(myPath, curveNumber)
    {
        activatePaper(canvasName);
        if(curveNumber >= myPath.curves.length  || curveNumber < 0) {
            return null
        }

        var myCurve= myPath.curves[curveNumber];

        var p0=myCurve.point1;
        var p1=helper.relativeToAbsoluteHandle(myCurve.point1, myCurve.handle1);
        var p2=helper.relativeToAbsoluteHandle(myCurve.point2, myCurve.handle2);
        var p3=myCurve.point2;

        var q0=p0;
        var r3=p3;

        var q1=helper.getMidPoint(p0,p1);
        var midPointTriangle = helper.getMidPoint(p1,p2);
        var r2=helper.getMidPoint(p2,p3);

        var q2=helper.getMidPoint(q1,midPointTriangle);
        var r1=helper.getMidPoint(midPointTriangle,r2);

        var q3=helper.getMidPoint(q2,r1);

        var r0=q3;

        var myMiddleSegment = new paper.Segment(q3,helper.absoluteToRelativeHandle(q3,q2),helper.absoluteToRelativeHandle(q3,r1));
        myPath.segments[curveNumber].handleOut = helper.absoluteToRelativeHandle(q0,q1);
        myPath.segments[curveNumber+1].handleIn = helper.absoluteToRelativeHandle(r3,r2);

        myPath.insert(curveNumber+1, myMiddleSegment);

        return myPath
    }

}
