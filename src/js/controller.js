var mypapers = [];

//canvas javascripts
var shapeDesigner;
var shapeStraightener;
var shapeProcesser;
var helper;
function initPaper(id, canvasElement) {
    mypapers[id] = new paper.PaperScope();
    paper = mypapers[id];
    paper.setup(canvasElement);
}

function activatePaper(id) {
    paper = mypapers[id];
}

$(document).ready(function() {
	helper = new HelperFunctions();


    initPaper("designCanvas", $("#designCanvas")[0]);
    initPaper("lineStraightenerCanvas", $("#lineStraightenerCanvas")[0]);
    initPaper("shapeProcessorCanvas", $("#shapeProcessorCanvas")[0]);

    shapeDesigner = new ShapeDesigner("designCanvas");
    shapeStraightener = new ShapeStraightener("lineStraightenerCanvas");
    shapeProcesser = new ShapeProcesser("shapeProcessorCanvas");

    shapeDesigner.init();
    shapeStraightener.init();
    shapeProcesser.init();
});

function flattenCurveIntoLineSegments()
{
    var flatnessThresholdRange = $("#flatnessThresholdRange")[0];
    var amount = flatnessThresholdRange.value;

    var flatnessThresholdDiv = $("#flatnessThresholdDiv")[0];
    flatnessThresholdDiv.innerHTML = amount;

    var svg = helper.getSVGPath(shapeDesigner.getPath());
    shapeStraightener.importPath(svg);
    shapeStraightener.processPath(amount);
}

function processLineSegmentsIntoFoldPattern()
{
    var svg = helper.getSVGPath(shapeStraightener.getPath());
    shapeProcesser.importPath(svg);
    shapeProcesser.processPath();
}

// event handlers
function flatnessThresholdRangeChanged()
{
    flattenCurveIntoLineSegments();
}
function processDesignBtnClicked()
{
    processLineSegmentsIntoFoldPattern();
}

