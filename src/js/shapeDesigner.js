/**
 * Created by Luke on 11/21/2015.
 */

var ShapeDesigner = function(myCanvasName) {

    var canvasName = myCanvasName;
	
//////////////// selection model //////////////
    var SelectionTypeEnum = {
        NothingSelected: 0,
        NodeSelected: 1,
        HandleOutSelected: 2,
        HandleInSelected: 3,
        BothHandlesSelected: 4
    }
    var SelectionItem = function () {
        this.selectionMode;
        this.selectedSegment;
        this.selectionPos;
        this.hasSavedUndoItem;
        this.reset = function () {
            this.selectionMode = SelectionTypeEnum.NothingSelected;
            this.selectedSegment = null;
            this.selectionPos = -1;
            this.hasSavedUndoItem = false;
        }
        this.set = function (selectionMode, segment, pos) {
            this.selectionMode = selectionMode;
            this.selectedSegment = segment;
            this.selectionPos = pos;
            this.hasSavedUndoItem = false;
        }
        this.isItemSelected = function () {
            if (this.selectionMode == SelectionTypeEnum.NothingSelected
                || this.selectedSegment == null
                || this.selectionPos == -1
            ) {
                this.reset();
                return false;
            }
            return true;
        }
        this.needToCreateUndoItem = function () {
            return !this.hasSavedUndoItem;
        }

        this.reset();
    };
//////////////// undo model ///////////////////
    var UntoTypeEnum = {
        UndoCreatePoint: 0,
        UndoDeletePoint: 1,
        UndoSegmentSelectionEdit: 2
    }
    var UndoAction = function (undoType, point, handleIn, handleOut, pos, selectionType) {
        this.undoType = undoType;
        this.point = point;
        this.handleIn = handleIn;
        this.handleOut = handleOut;
        this.pos = pos;
        this.selectionType = selectionType;
    };
//////////////// members ///////////////////
    var undoStack = [];
    var visiblePoints = [];

    var path;
    var nodeSymbol;
    var myTool;
    var forceStrightLines = false;

    var currentlySelectedItem = new SelectionItem();
    currentlySelectedItem.reset();

    this.getPath = function()
    {
        return path;
    }
///////////////// init ///////////////////////
    this.init = function () {
        activatePaper(canvasName);

        //start the drawing path
        path = new paper.Path();
        path.strokeColor = 'black';

        //create the point circles
        var nodePath = new paper.Path.Circle(new paper.Point(20, 20), 4);
        nodePath.strokeColor = 'black';
        nodePath.remove();
        nodeSymbol = new paper.Symbol(nodePath);

        myTool = new paper.Tool();
        myTool.parentCanvas = this;

        this.updateUI();

///////////// PaperJS eventhandlers ////////////////////
        myTool.onMouseDown = function (event) {
            activatePaper(canvasName);

            var clickSize = 10;
            var closeToPointRect = new paper.Rectangle(event.point.x - clickSize / 2, event.point.y - clickSize / 2, clickSize, clickSize);

            if (currentlySelectedItem.isItemSelected() && currentlySelectedItem.needToCreateUndoItem()) {
                //var absPosOfHandleOutX = currentlySelectedItem.selectedSegment.handleOut.x + currentlySelectedItem.selectedSegment.point.x;
                //var absPosOfHandleOutY = currentlySelectedItem.selectedSegment.handleOut.y + currentlySelectedItem.selectedSegment.point.y;
                //var absPointOfHandleOut = new paper.Point(absPosOfHandleOutX, absPosOfHandleOutY);
				var absPointOfHandleOut = helper.relativeToAbsoluteHandle(
											currentlySelectedItem.selectedSegment.point, 
											currentlySelectedItem.selectedSegment.handleOut
										);
				var absPointOfHandleIn = helper.relativeToAbsoluteHandle(
											currentlySelectedItem.selectedSegment.point, 
											currentlySelectedItem.selectedSegment.handleIn
										);
                //var absPosOfHandleInX = currentlySelectedItem.selectedSegment.handleIn.x + currentlySelectedItem.selectedSegment.point.x;
                //var absPosOfHandleInY = currentlySelectedItem.selectedSegment.handleIn.y + currentlySelectedItem.selectedSegment.point.y;
                //var absPointOfHandleIn = new paper.Point(absPosOfHandleInX, absPosOfHandleInY);

                if (absPointOfHandleOut.isInside(closeToPointRect)) {
                    currentlySelectedItem.selectionMode = SelectionTypeEnum.HandleOutSelected;
                    return;
                }
                else if (absPointOfHandleIn.isInside(closeToPointRect)) {
                    currentlySelectedItem.selectionMode = SelectionTypeEnum.HandleInSelected;
                    return;
                }

            }
            this.parentCanvas.finishPointSelection();

            for (var i = path.segments.length - 1; i > -1; i--) {
                if (path.segments[i].point.isInside(closeToPointRect)) {
                    currentlySelectedItem.set(SelectionTypeEnum.NodeSelected, path.segments[i], i);
                    this.parentCanvas.visuallySelectSegment();
                    return;
                }
            }

            //must have clicked on empty area, so create a new point
            this.parentCanvas.createPoint(event.point);

        }
        myTool.onMouseMove = function (event) {
            activatePaper(canvasName);

        }
        myTool.onMouseDrag = function (event) {
            activatePaper(canvasName);

            this.parentCanvas.movePointPosition(event.point);
        }
        myTool.onMouseUp = function (event) {
            activatePaper(canvasName);

            currentlySelectedItem.hasSavedUndoItem = false;
            //do nothing
        }
    }

/////////////// path creation functions ///////////////
    this.createPoint = function(point) {
        activatePaper(canvasName);

        var pos = path.segments.length;

        var instance1 = nodeSymbol.place();
        instance1.position = point;
        visiblePoints[pos] = instance1;
        path.add(point);
        this.pushUndoStack(new UndoAction(UntoTypeEnum.UndoCreatePoint, point, null, null, pos, SelectionTypeEnum.BothHandlesSelected));

        if (forceStrightLines == false) {
            currentlySelectedItem.set(SelectionTypeEnum.BothHandlesSelected, path.segments[pos], pos);
            this.visuallySelectSegment();
        }
        currentlySelectedItem.hasSavedUndoItem = true;

        this.updateUI();
    }

     this.movePointPosition = function(point) {
         activatePaper(canvasName);

         if (currentlySelectedItem.isItemSelected() == false) {
            return;
        }

        if (currentlySelectedItem.needToCreateUndoItem()) {
            currentlySelectedItem.hasSavedUndoItem = true;

            var copiedPoint = new paper.Point(currentlySelectedItem.selectedSegment.point.x, currentlySelectedItem.selectedSegment.point.y);
            var copiedHandleIn = new paper.Point(currentlySelectedItem.selectedSegment.handleIn.x, currentlySelectedItem.selectedSegment.handleIn.y);
            var copiedHandleOut = new paper.Point(currentlySelectedItem.selectedSegment.handleOut.x, currentlySelectedItem.selectedSegment.handleOut.y);

            this.pushUndoStack(new UndoAction(
                UntoTypeEnum.UndoSegmentSelectionEdit,
                copiedPoint,
                copiedHandleIn,
                copiedHandleOut,
                currentlySelectedItem.selectionPos,
                currentlySelectedItem.selectionMode)
            );
        }

        var deltaXFromNode = point.x - currentlySelectedItem.selectedSegment.point.x;
        var deltaYFromNode = point.y - currentlySelectedItem.selectedSegment.point.y;

        switch (currentlySelectedItem.selectionMode) {
            case SelectionTypeEnum.NodeSelected:
                currentlySelectedItem.selectedSegment.point = point;
                visiblePoints[currentlySelectedItem.selectionPos].position = point;
                break
            case SelectionTypeEnum.HandleOutSelected:
                currentlySelectedItem.selectedSegment.handleOut = new paper.Point(deltaXFromNode, deltaYFromNode);
                break
            case SelectionTypeEnum.HandleInSelected:
                currentlySelectedItem.selectedSegment.handleIn = new paper.Point(deltaXFromNode, deltaYFromNode);
                break
            case SelectionTypeEnum.BothHandlesSelected:
                currentlySelectedItem.selectedSegment.handleOut = new paper.Point(deltaXFromNode, deltaYFromNode);
                currentlySelectedItem.selectedSegment.handleIn = new paper.Point(-1 * deltaXFromNode, -1 * deltaYFromNode);
                break
            default:
        }
    }

     this.finishPointSelection = function() {
         activatePaper(canvasName);

         this.visuallyUnselectSegment();
        currentlySelectedItem.reset();
    }

/////////////// path creation unto functions ///////////////
    this.popUndoStack = function() {
        activatePaper(canvasName);

        if (undoStack.length == 0) {
            this.updateUI();
            return;
        }

        var element = undoStack.pop();
        switch (element.undoType) {
            case UntoTypeEnum.UndoCreatePoint:
                path.removeSegment(element.pos);
                visiblePoints[element.pos].remove();
                visiblePoints[element.pos] = null;
                break
            case UntoTypeEnum.UndoDeletePoint:

                break
            case UntoTypeEnum.UndoSegmentSelectionEdit:
                path.segments[element.pos].point = element.point;
                path.segments[element.pos].handleIn = element.handleIn;
                path.segments[element.pos].handleOut = element.handleOut;
                visiblePoints[element.pos].position = element.point;
                this.finishPointSelection();
                currentlySelectedItem.set(element.selectionType, path.segments[element.pos], element.pos);
                this.visuallySelectSegment();
                break
            default:
        }

        this.updateUI();
        return element;
    }

    this.pushUndoStack = function(element) {
        activatePaper(canvasName);

        undoStack.push(element);
        this.updateUI();
    }

//////////////// helper functions /////////////////
     this.visuallySelectSegment = function() {
         activatePaper(canvasName);

         if (currentlySelectedItem.isItemSelected()) {
            if (path.segments[currentlySelectedItem.selectionPos] != null) {
                path.segments[currentlySelectedItem.selectionPos].selected = true;
            }
        }

    }

    this.visuallyUnselectSegment = function() {
        activatePaper(canvasName);

        if (currentlySelectedItem.isItemSelected()) {
            if (path.segments[currentlySelectedItem.selectionPos] != null) {
                path.segments[currentlySelectedItem.selectionPos].selected = false;
            }
        }
    }

    this.updateUI = function() {
        activatePaper(canvasName);

		var undoButton = document.getElementById("undoBtn");
        paper.view.draw();
        if (undoStack.length > 0) {
            undoButton.disabled = false;
        }
        else {
            undoButton.disabled = true;
        }
    }

}

