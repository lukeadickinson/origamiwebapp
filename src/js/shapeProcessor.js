/**
 * Created by Luke on 11/21/2015.
 */
var ShapeProcesser = function(myCanvasName) {

    var canvasName = myCanvasName;

    var svgOrginal;

    var path;
    var leftTracePath;
    var rightTracePath;

    var leftParallelPaths = [];
    var rightParallelPaths = [];
    var leftTracePoints = [];
    var rightTracePoints = [];

    var shapePaths = [];

    var leftPathColor ="#00ff00";
    var rightPathColor ="#FF0000";
    var defaultPathColor ="black";



    this.getPath = function()
    {
        return path;
    }

    this.init = function(){
        activatePaper(canvasName);
    }
	this.importPath = function (svgPath){
        activatePaper(canvasName);
        svgOrginal = svgPath;
        paper.project.clear();
        path = paper.project.importSVG(svgPath);
        path.strokeWidth = 2;
        paper.view.draw();
    }
	this.processPath = function (){
        activatePaper(canvasName);

        if(path != null) {
            this.drawParallelLines(path);
            this.traceParellelLines(true);
            this.traceParellelLines(false);

            paper.project.clear();

            path = paper.project.importSVG(svgOrginal);
            path.strokeWidth = 2;

            //leftTracePath = new paper.Path(leftTracePoints);
            //rightTracePath= new paper.Path(rightTracePoints);

            //leftTracePath.strokeWidth = 2;
            //leftTracePath.strokeColor= leftPathColor;

            //rightTracePath.strokeWidth = 2;
            //rightTracePath.strokeColor= rightPathColor;

            this.createShapes(leftTracePoints,rightTracePoints);
        }

        paper.view.draw();
    }
    this.drawParallelLines = function(myPath)
    {
        activatePaper(canvasName);

        leftParallelPaths = [];
        rightParallelPaths = [];

        var spaceBetween = 10;
        for (index = 0, len = myPath.curves.length; index < len; ++index){
            var myCurve = myPath.curves[index];
            var x1 = myCurve.point1.x
            var y1 = myCurve.point1.y
            var x2 = myCurve.point2.x
            var y2 = myCurve.point2.y

            var valueToUse = spaceBetween;
            var slope = 1.0;

            if(x1 == x2){
                slope = Number.POSITIVE_INFINITY;
            }
            else
            {
                slope = (y2 - y1) / (x2 - x1);
            }
            if(Math.abs(slope) >= 1)
            {
                var difference = y2 - y1;
                if(difference>0){
                    valueToUse *= -1;
                }
            }
            else{
                var difference = x2 - x1;
                if(difference<=0){
                    valueToUse *= -1;
                }
            }

            var leftLine = this.createParallelLine(myCurve, -1*valueToUse, leftPathColor);
            leftLine = this.lengthenLine(leftLine,-1*valueToUse, index==0, index== len-1);
            leftParallelPaths.push(leftLine);

            var rightLine = this.createParallelLine(myCurve, valueToUse, rightPathColor);
            rightLine = this.lengthenLine(rightLine,valueToUse, index==0,index== len-1);
            rightParallelPaths.push(rightLine);
        }

    }

    this.createParallelLine = function(myCurve, spaceBetween, myColor)
    {
        activatePaper(canvasName);

        var x1 = myCurve.point1.x
        var y1 = myCurve.point1.y
        var x2 = myCurve.point2.x
        var y2 = myCurve.point2.y

        if (x1 == x2)
        {
            x1 += spaceBetween;
            x2 += spaceBetween;
        }
        else
        {
            var slope = (y2 - y1) / (x2 - x1);

            //move the line up or down
            if (slope < 1 && slope >= -1)
            {
                y1 += spaceBetween;
                y2 += spaceBetween;

            }
            else
            {
                x1 += spaceBetween;
                x2 += spaceBetween;
            }
        }

        var myPath = new paper.Path([new paper.Point(x1,y1), new paper.Point(x2,y2)]);
        myPath.strokeColor = myColor;
        return myPath;
    }
    this.lengthenLine = function(myPath, spaceBetween, isFirstNode, isLastNode) {
        activatePaper(canvasName);

        var slope = 1.0

        var myCurve = myPath.firstCurve;

        var x1 = myCurve.point1.x
        var y1 = myCurve.point1.y
        var x2 = myCurve.point2.x
        var y2 = myCurve.point2.y
        var absSpaceBetween = Math.abs(spaceBetween) * 3;

        if (x1 == x2) {
            //we have a vertical line, we only need to move left or right
            //also we don't want mix infinities with other calculations
            slope = Number.POSITIVE_INFINITY;

            if (y1 < y2) {
                if (!isFirstNode)
                    y1 -= absSpaceBetween;
                if (!isLastNode)
                    y2 += absSpaceBetween;
            }
            else {
                if (!isFirstNode)
                    y1 += absSpaceBetween;
                if (!isLastNode)
                    y2 -= absSpaceBetween;
            }
        }
        else {
            //we don't a vertical line. We can calculate slope and use it

            slope = (y2 - y1) / (x2 - x1);

            var x = Math.sqrt(
                Math.pow(absSpaceBetween, 2) / (Math.pow(slope, 2) + 1)
            );
            var y = Math.sqrt(
                Math.pow(absSpaceBetween, 2) - Math.pow(x, 2)
            );

            //add the length calculated onto the correct side

            if (y1 < y2) {
                if (!isFirstNode)
                    y1 -= y;
                if (!isLastNode)
                    y2 += y;
            }
            else {
                if (!isFirstNode)
                    y1 += y;
                if (!isLastNode)
                    y2 -= y;
            }

            if (x1 < x2) {
                if (!isFirstNode)
                    x1 -= x;
                if (!isLastNode)
                    x2 += x;
            }
            else {
                if (!isFirstNode)
                    x1 += x;
                if (!isLastNode)
                    x2 -= x;
            }
        }
            myPath.firstCurve.point1.x = x1;
            myPath.firstCurve.point1.y = y1;
            myPath.firstCurve.point2.x = x2;
            myPath.firstCurve.point2.y = y2;

            return myPath;
    }
    this.traceParellelLines = function(startWithLeft)
    {
        activatePaper(canvasName);

        var onLeft = startWithLeft;
        //first point
        if (startWithLeft){
            leftTracePoints = [];
            var firstLine = leftParallelPaths[0].firstCurve;
            var startingPoint = new paper.Point(firstLine.point1.x, firstLine.point1.y);
            leftTracePoints.push(startingPoint);
        }
        else {
            rightTracePoints = [];
            var firstLine = rightParallelPaths[0].firstCurve;
            var startingPoint = new paper.Point(firstLine.point1.x, firstLine.point1.y);
            rightTracePoints.push(startingPoint);
        }
        for (index = 1, len = leftParallelPaths.length; index < len; ++index){
            var line1;
            var line2;
            if (onLeft)
                line1 = leftParallelPaths[index - 1].firstCurve;
            else
                line1 = rightParallelPaths[index - 1].firstCurve;

            if (!onLeft)
                line2 = leftParallelPaths[index].firstCurve;
            else
                line2 = rightParallelPaths[index].firstCurve;

            if (startWithLeft)
                leftTracePoints.push(this.intersectingPoint(line1, line2));
            else
                rightTracePoints.push(this.intersectingPoint(line1, line2));

            onLeft = !onLeft;
        }

        var lastPointToAdd;

        if(onLeft)
        {
            lastPointToAdd = new paper.Point(
                leftParallelPaths[leftParallelPaths.length - 1].firstCurve.point2.x,
                leftParallelPaths[leftParallelPaths.length - 1].firstCurve.point2.y
            );
        }
        else{
            lastPointToAdd = new paper.Point(
                rightParallelPaths[rightParallelPaths.length - 1].firstCurve.point2.x,
                rightParallelPaths[rightParallelPaths.length - 1].firstCurve.point2.y
            );
        }

        if (startWithLeft)
        {
            leftTracePoints.push(lastPointToAdd);
        }
        else
        {
            rightTracePoints.push(lastPointToAdd);
        }
    }
    this.intersectingPoint = function(line1, line2)
    {
        activatePaper(canvasName);

        var intersectingY =0;
        var intersectingX =0;

        if(line1.point1.x == line1.point2.x)
        {
            //infinite slope1

            //slope 2 is not infinite as they will cross
            var slope2 = (line2.point2.y - line2.point1.y)
                        / (line2.point2.x - line2.point1.x);
            var b2 =line2.point1.y - (line2.point1.x*slope2);
            intersectingX = line1.point1.x;
            intersectingY = intersectingX*slope2 + b2;
        }
        else if(line2.point1.x == line2.point2.x)
        {
            //infinite slope2
            //slope 1 is not infinite as they will cross

            var slope1 = (line1.point2.y - line1.point1.y)
                / (line1.point2.x - line1.point1.x);
            var b1 =line1.point1.y - (line1.point1.x*slope1);

            intersectingX = line2.point1.x;
            intersectingY = intersectingX*slope1 + b1;
        }
        else{
            //nether slope is infinite
            var slope1 = (line1.point2.y - line1.point1.y)
                / (line1.point2.x - line1.point1.x);
            var slope2 = (line2.point2.y - line2.point1.y)
                / (line2.point2.x - line2.point1.x);
            var b1 =line1.point1.y - (line1.point1.x*slope1);
            var b2 =line2.point1.y - (line2.point1.x*slope2);

            intersectingX = (b2 - b1) / (slope1 - slope2);
            intersectingY = intersectingX*slope1 + b1;
        }
        return new paper.Point(intersectingX, intersectingY);
    }
    this.createShapes = function(myLeftTracePoints, myRightTracePoints) {
        activatePaper(canvasName);

        shapePaths = [];
        var isFoldSide1 = true;
        for (index = 1, len = myLeftTracePoints.length; index < len; ++index) {
            {
                var point1 = myLeftTracePoints[index - 1];
                var point2 = myRightTracePoints[index - 1];
                var point3 = myRightTracePoints[index];
                var point4 = myLeftTracePoints[index];

                var myShape = new paper.Path([point1,point2,point3,point4]);
                myShape.closed = true;
                myShape.strokeWidth =2;
                if(isFoldSide1){
                    myShape.strokeColor = leftPathColor;
                }
                else {
                    myShape.strokeColor = rightPathColor;
                }
                isFoldSide1 = !isFoldSide1;
                shapePaths.push(myShape);
            }
        }
    }
}
