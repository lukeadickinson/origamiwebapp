var Drawing2 = function(myCanvasName) {

    var canvasName = myCanvasName;
    var path;
    var text;

    this.init = function() {
        //nothing needed
    }
    this.drawSquare = function(){

        activatePaper(canvasName);

        if (path) {
            path.remove();
            text.remove();
        }
        var point = new paper.Point(100, 100);
        var size = new paper.Size(100, 100);
        var rectangle = new paper.Rectangle(point, size);
        var cornerSize = new paper.Size(30, 30);
        path = new paper.Path.RoundRectangle(rectangle, cornerSize);

        path.fillColor = '#' + Math.floor(Math.random() * 16777215).toString(16);

        text = new paper.PointText(new paper.Point(150, 90));
        text.justification = 'center';
        text.fillColor = 'black';
        text.content = 'This is canvas 2';

        paper.view.draw();
    }

}
