/**
 * Created by Luke on 11/21/2015.
 */
var HelperFunctions = function() {
    this.absoluteToRelativeHandle = function(myPoint, myAbsHandle)
    {
        var relativePosOfHandlePoint = new paper.Point(myAbsHandle.x - myPoint.x, myAbsHandle.y - myPoint.y);
        return relativePosOfHandlePoint;
    }
    this.relativeToAbsoluteHandle = function(myPoint, myRelHandle)
    {
        var absolutePosOfHandlePoint = new paper.Point(myPoint.x + myRelHandle.x, myPoint.y + myRelHandle.y);
        return absolutePosOfHandlePoint;
    }
    this.getMidPoint = function(point1, point2)
    {
        var myMidPoint = new paper.Point((point1.x + point2.x)/2, (point1.y + point2.y)/2);
        return myMidPoint;
    }
    this.getLengthOfLine = function(point1, point2)
    {
        var distance = point1.getDistance(point2);
        return distance;
    }
    this.drawColoredFirstAlternatePath = function (myCanvas, myPath, color){
        activatePaper(myCanvas);
        for (index = 1, len = myPath.segments.length; index < len; index+=2)
        {
            var mySegment1 = myPath.segments[index-1];
            var mySegment2 = myPath.segments[index];

            var coloredLine = new paper.Path([mySegment1,mySegment2]);
            coloredLine.strokeColor =color;
            coloredLine.strokeWidth = 2;
        }
        paper.view.draw();
    }
    this.drawColoredSecondAlternatePath = function (myCanvas, myPath, color){
        activatePaper(myCanvas);
        for (index = 2, len = myPath.segments.length; index < len; index+=2)
        {
            var mySegment1 = myPath.segments[index-1];
            var mySegment2 = myPath.segments[index];

            var coloredLine = new paper.Path([mySegment1,mySegment2]);
            coloredLine.strokeColor =color;
            coloredLine.strokeWidth = 2;
        }
        paper.view.draw();
    }
    this.getSVGPath = function(myPath)
    {
        return myPath.exportSVG()
    }
    this.pathExists = function(myPath)
    {
        if(myPath.segements.length > 1){
            return true;
        }
        return false;
    }
}